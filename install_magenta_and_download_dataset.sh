echo "Installing magenta to ./magenta/ ..."
git clone https://github.com/gudgud96/magenta magenta-repo
mv -i magenta-repo/magenta ./
rm -rf magenta-repo

echo "Downloading dataset download script to scripts/ ..."
mkdir -p scripts/
curl -o scripts/ecomp_piano_downloader.sh -L https://raw.githubusercontent.com/adamoudad/MusicTransformer-tensorflow2.0/master/dataset/scripts/ecomp_piano_downloader.sh

echo "Downloading dataset to dataset/..."
bash scripts/ecomp_piano_downloader.sh ./dataset/
